const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  publicPath: process.env.NODE_ENV === 'production'
    ? './'
    : './',

  chainWebpack: config => {
    config.plugin('html').tap(args => {
      var c = require("./src/assets/config.json");
      args[0].title = c.appName;
      return args;
    })
  },

  configureWebpack: {
    devtool: 'source-map',
    plugins: [
      new CopyWebpackPlugin([{
        from: "./src/assets/config.json",
        to: "./",
      }])
    ]
  },

  devServer: {
    //https: true
  },
}